# Netatmo Weather for Garmin watches

Work in Progress

## Description

Displays readings from  your Netatmo Weather Station.

## Usage

### Setup

1. Install the app using Garmin Connect IQ
2. Launch the app from your device
3. Hold the settings menu on your device to bring up app settings
4. Click the "Login" setting menu item to use your phone to authenticate with the Netatmo Api.

## Supported Devices

Development is tested on the Venu line of devices

## License

This code is MIT licensed `Copyright 2024 by DJ Mountney`. See the LICENSE file for the full text.

The `sources/WrapText.mc` is CC 4.0 licensed `Copyright 2017 by HarryOnline`.

Icons are from the [Ionic icon pack](https://github.com/ionic-team/ionicons).
