import Toybox.Lang;
import Toybox.Time;
import Toybox.Application;
using Toybox.System;
using Toybox.Timer;

(:glance)
class RemoteDataController {
    hidden var requestTimer;
    hidden var currentData;
    hidden var cachedData;
    hidden var requestListeners as Dictionary<String, Method>;

    function initialize() {
        requestListeners = {};
        cachedData = Storage.getValue("cachedStationData");
        Application.getApp().tokens.addAuthListener("remote-data", method(:start));
    }

    function addRequestListener(key as String, handler as Method) {
        requestListeners[key] = handler;
    }

    function removeRequestListener(key as String) {
        if (requestListeners.hasKey(key)) {
            requestListeners.remove(key);
        }
    }

    function runRequestListeners() {
        var toRun = requestListeners.values();
        for( var i = 0; i < toRun.size(); i++ ) {
            toRun[i].invoke();
        }
    }

    function start() {
        fetchStationData();
        if (requestTimer == null) {
            requestTimer = new Timer.Timer();
            requestTimer.start(method(:fetchStationData), 300 * 1000, true);
        }
    }

    function getData() as WeatherData or Null {
        if (currentData != null) {
            return new WeatherData(currentData);
        }
        return currentData;
    }

    function getDataOrCache() as WeatherData or Null {
        if (currentData != null) {
            return new WeatherData(currentData);
        } else if (cachedData != null) {
            return new WeatherData(cachedData);
        }

        return null;
    }

    function fetchStationData() as Void {
        if(System.getDeviceSettings().connectionAvailable == false) {
            return;
        }
        var currentToken = Application.getApp().tokens.grabToken();
        if (currentToken) {
            Communications.makeWebRequest(
                "https://api.netatmo.com/api/getstationsdata",
                { "get_favorites" => "false"},
                {
                    :method => Communications.HTTP_REQUEST_METHOD_GET,
                    :headers => {
                        "Authorization" => "Bearer " + (currentToken.access_token as String)
                    }
                },
                method(:onGetStationData)
            );
        }
    }

    function onGetStationData(responseCode as Number, data as Null or Dictionary) as Void {
        if (responseCode == 200) {
            // System.println(data);
            currentData = data;
            Storage.setValue("cachedStationData", data);
            runRequestListeners();

        } else {
            System.println("Get Station Data Response: " + responseCode);
            System.println(data);
        }
    }
}