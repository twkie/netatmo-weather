import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;

module Netatmo {
    const LOCATION_TYPE_OUTSIDE = 0;
    const LOCATION_TYPE_INSIDE = 1;
}

class NetatmoWeatherApp extends Application.AppBase {

    var tokens;
    var remote;
    var display;
    var glanceDisplay;

    const SIM_UID = "6eb3ed428dc4c73cc15d9affae55263e5e274c20";

    function initialize() {
        AppBase.initialize();
        initAppSetup();
        tokens = new TokenController();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
        remote = new RemoteDataController();
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
        if (display != null) {
            display.stop();
        }
        if (glanceDisplay != null) {
            glanceDisplay.stop();
        }
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        display = new DisplayController();
        if(tokens.hasToken()) {
            remote.start();
        }
        display.start();
        return [ new InitialView(tokens.hasToken()), new InitialViewDelegate() ] as Array<Views or InputDelegates>;
    }

    function getGlanceView() as Array<WatchUi.GlanceView or GlanceViewDelegate> or Null {
        glanceDisplay = new GlanceDisplayController();
        if(tokens.hasToken()) {
            remote.start();
        }
        glanceDisplay.start();
        return [new NetatmoWeatherGlanceView()];
    }

    (:debug) function initAppSetup() {
        System.println("App Unique Id: " + System.getDeviceSettings().uniqueIdentifier);
    }

    (:release) function initAppSetup() {
        // nothing for now
    }

}

function getApp() as NetatmoWeatherApp {
    return Application.getApp() as NetatmoWeatherApp;
}