import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Communications;
import Toybox.Application.Storage;
import Toybox.Lang;
import Toybox.Timer;

class NetatmoWeatherView extends WatchUi.View {
    var pageIndex as Number or Null;
    var numPages = 0;
    var locationType = Netatmo.LOCATION_TYPE_OUTSIDE;

    function initialize(page as Number, location as Number) {
        pageIndex = page;
        locationType = location;
        View.initialize();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {

    }

    function goNextTab() as Void {
        var nextPage = pageIndex + 1;
        if (nextPage >= numPages) {
            nextPage = 0;
        }
        WatchUi.switchToView(
            new NetatmoWeatherView(nextPage, locationType),
            new NetatmoWeatherDelegate(),
            WatchUi.SLIDE_LEFT
        );
    }

    function goPreviousTab() as Void {
        var nextPage = pageIndex - 1;
        if (nextPage < 0) {
            nextPage = numPages - 1;
        }
        WatchUi.switchToView(
            new NetatmoWeatherView(nextPage, locationType),
            new NetatmoWeatherDelegate(),
            WatchUi.SLIDE_RIGHT
        );
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        var data = Application.getApp().remote.getData();
        if (data != null) {
            dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
            dc.clear();
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

            if (locationType == Netatmo.LOCATION_TYPE_OUTSIDE) {
                renderOutsideInfo(dc, data);
            } else {
                renderInsideInfo(dc, data);
            }

        } else {
            // default layout
            View.onUpdate(dc);
        }
    }

    function renderOutsideInfo(dc, data) as Void {
        var outsideModules = data.getOutsideModules() as Array<WeatherModule>;
        numPages = outsideModules.size();
        var outsideMod = outsideModules[pageIndex];
        var tinyFontHeight = Graphics.getFontHeight(Graphics.FONT_XTINY);
        if (outsideMod != null && outsideMod.temp != null) {
            drawModuleName(dc, outsideMod.name);
            dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_NUMBER_MEDIUM, " " + outsideMod.temp.format("%.1f") + "°", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            if (outsideMod.humidity != null) {
                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
                dc.drawText(dc.getWidth() * 2 / 3, dc.getHeight() * 2 / 3, Graphics.FONT_XTINY, "Humidity", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
                dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
                dc.drawText(dc.getWidth() * 2 / 3, (dc.getHeight() * 2 / 3) + tinyFontHeight, Graphics.FONT_XTINY, outsideMod.humidity.format("%d") + "%", Graphics.TEXT_JUSTIFY_CENTER| Graphics.TEXT_JUSTIFY_VCENTER);
            }
        } else {
            dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_MEDIUM, "No data", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        }
    }

    function renderInsideInfo(dc, data) as Void {
        var insideModules = data.getInsideModules() as Array<WeatherModule>;
        numPages = insideModules.size();
        var insideMod = insideModules[pageIndex];
        var tinyFontHeight = Graphics.getFontHeight(Graphics.FONT_XTINY);
        if (insideMod != null && insideMod.temp != null) {
            drawModuleName(dc, insideMod.name);
            dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_NUMBER_MEDIUM, " " + insideMod.temp.format("%.1f") + "°", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            if (insideMod.humidity != null) {
                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
                dc.drawText((dc.getWidth() * 2 / 3), dc.getHeight() * 2 / 3, Graphics.FONT_XTINY, "Humidity", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
                dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
                dc.drawText((dc.getWidth() * 2 / 3), (dc.getHeight() * 2 / 3) + tinyFontHeight, Graphics.FONT_XTINY, insideMod.humidity.format("%d") + "%", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            }
            if (insideMod.co2 != null) {
                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
                dc.drawText((dc.getWidth() / 3) - 5, dc.getHeight() * 2 / 3, Graphics.FONT_XTINY, "CO2", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
                dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
                dc.drawText((dc.getWidth() / 3) - 5, (dc.getHeight() * 2 / 3) + tinyFontHeight, Graphics.FONT_XTINY, insideMod.co2.format("%d") + "ppm", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            }
        } else {
            dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_MEDIUM, "No data", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        }
    }

    function drawModuleName(dc, name as String) {
        var textToDraw = Graphics.fitTextToArea(name, Graphics.FONT_MEDIUM, dc.getWidth(), Graphics.getFontHeight(Graphics.FONT_MEDIUM), true);
        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 3, Graphics.FONT_MEDIUM, textToDraw, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {

    }

}
