import Toybox.WatchUi;

class SettingsMenu extends Menu2 {
    function initialize() {
        Menu2.initialize(null);
        Menu2.setTitle("Settings");
        Menu2.addItem(new MenuItem("Login", null, "auth", null));
    }
}

class SettingsMenuDelegate extends Menu2InputDelegate {
    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(menuItem) {
        switch(menuItem.getId()) {
            case "auth":
                Application.getApp().tokens.addAuthListener("settings-menu", method(:gotAuthMessage));
                var launchAuth = Application.getApp().tokens.doAuth();
                if(launchAuth) {
                    WatchUi.pushView(
                        new Action("Launched login on phone.", "settings-auth", method(:removeAuthListener)),
                        null,
                        WatchUi.SLIDE_IMMEDIATE
                    );
                } else {
                    WatchUi.pushView(
                        new Action("Phone not connected.", "settings-error", null),
                        null,
                        WatchUi.SLIDE_IMMEDIATE
                    );
                }
            break;
        }
    }

    function removeAuthListener() as Void {
        Application.getApp().tokens.removeAuthListener("settings-menu");
    }

    function gotAuthMessage() {
        var view = WatchUi.getCurrentView()[0];
        if(view instanceof Action) {
            var actionView = view as Action;
            if(actionView.getContext().equals("settings-auth")) {
                WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
            }
        }
    }
}