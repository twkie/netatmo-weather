import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;
import Rez.Styles;

class InitialViewDelegate extends WatchUi.BehaviorDelegate {
    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onKey(evt as KeyEvent) as Boolean {
        if (Styles.system_input__action_menu has :button &&
            evt.getKey() == Styles.system_input__action_menu.button) {
            onMenu();
            return true;
        }
        return false;
    }

    function onTap(evt as ClickEvent) as Boolean {
        if (!(Styles.system_input__action_menu has :button) && $.isInActionArea(evt.getCoordinates())) {
            onMenu();
            return true;
        }
        return false;
    }

    (:appsettings)
    function onMenu() {
        WatchUi.pushView(
            new SettingsMenu(),
            new SettingsMenuDelegate(),
            WatchUi.SLIDE_IMMEDIATE
        );

        return true;
    }
}