import Toybox.WatchUi;
import Toybox.Timer;
import Toybox.Lang;

class DisplayController {
    hidden var dTimer;
    hidden var isLoaded = false;

    function initialize() {
        dTimer = new Timer.Timer();
        Application.getApp().remote.addRequestListener("display-controller", method(:onLoadingComplete));
    }

    function start() {
        dTimer.start(method(:onTimer), 5 * 1000, true);
    }

    function onTimer() {
        WatchUi.requestUpdate();
    }

    function stop() {
        dTimer.stop();
    }

    function onLoadingComplete() {
        Application.getApp().remote.removeRequestListener("display-controller");
        isLoaded = true;
        var view = WatchUi.getCurrentView()[0];
        if(view instanceof WatchUi.ProgressBar) {
            WatchUi.popView(WatchUi.SLIDE_BLINK);
        } else {
            WatchUi.requestUpdate();
        }
    }

    function getFirstOutdoorView() as WatchUi.View {
        return new NetatmoWeatherView(0, Netatmo.LOCATION_TYPE_OUTSIDE);
    }

    function onTogglePage(transition) as Boolean {
        var view = WatchUi.getCurrentView()[0];
        if(view instanceof NetatmoWeatherView) {
            if (view.locationType == Netatmo.LOCATION_TYPE_OUTSIDE) {
                WatchUi.pushView(
                    new NetatmoWeatherView(0, Netatmo.LOCATION_TYPE_INSIDE),
                    new NetatmoWeatherDelegate(),
                    transition
                );
            } else {
                WatchUi.popView(transition);
            }
            return true;
        }
        return false;
    }

    function onNextTab() as Boolean {
        var view = WatchUi.getCurrentView()[0];
        if(view instanceof NetatmoWeatherView) {
            view.goNextTab();
            return true;
        }
        return false;
    }

    function onPreviousTab() {
        var view = WatchUi.getCurrentView()[0];
        if(view instanceof NetatmoWeatherView) {
            view.goPreviousTab();
            return true;
        }
        return false;
    }
}