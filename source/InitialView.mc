import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;

class InitialView extends WatchUi.View {
    hidden var startLoading = false;
    function initialize(loading as Boolean) {
        startLoading = loading;
        View.initialize();
    }

    function onLayout(dc as Dc) as Void {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    function onUpdate(dc as Dc) as Void {
        var data = Application.getApp().remote.getData();
        if (data != null) {
            WatchUi.switchToView(
                Application.getApp().display.getFirstOutdoorView(),
                new NetatmoWeatherDelegate(),
                WatchUi.SLIDE_BLINK
            );
        } else {
            View.onUpdate(dc);
            var mainLabel = View.findDrawableById("mainLabel") as WatchUi.TextArea;
            if (Application.getApp().tokens.hasToken() && mainLabel != null) {
                mainLabel.setText("No Data Available.");
                mainLabel.draw(dc);
            }
        }
    }

    function onShow() as Void {
        if(startLoading) {
            startLoading = false;
            if(System.getDeviceSettings().phoneConnected == false) {
                WatchUi.pushView(
                    new Action("Phone not connected.", "settings-error", null),
                    null,
                    WatchUi.SLIDE_BLINK
                );
            } else {
                WatchUi.pushView(new WatchUi.ProgressBar("Loading...", null) , null, WatchUi.SLIDE_BLINK);
            }
        }
    }
}