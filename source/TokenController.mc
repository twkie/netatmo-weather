import Toybox.Lang;
import Toybox.Time;
import Toybox.Communications;
import Toybox.Application.Storage;
import Toybox.System;
using Toybox.Cryptography as Crypt;

(:glance)
class TokenController {
    hidden var currentToken as Token or Null;
    hidden var lastError;
    hidden var authListeners as Dictionary<String, Method>;

    function initialize() {
        authListeners = {};
        Communications.registerForOAuthMessages(method(:onAuthCode));
        if(Storage.getValue("oAuthToken")) {
            currentToken = new Token(Storage.getValue("oAuthToken"));
        }
    }

    function randomBase64() as String {
        return StringUtil.convertEncodedString(
            Crypt.randomBytes(16),
            {
                :fromRepresentation => StringUtil.REPRESENTATION_BYTE_ARRAY,
                :toRepresentation => StringUtil.REPRESENTATION_STRING_BASE64,
                :encoding => StringUtil.CHAR_ENCODING_UTF8
            }
        ).toString();
    }

    function addAuthListener(key as String, handler as Method) {
        authListeners[key] = handler;
    }

    function removeAuthListener(key as String) {
        if (authListeners.hasKey(key)) {
            authListeners.remove(key);
        }
    }

    function runAuthListeners() {
        var toRun = authListeners.values();
        for( var i = 0; i < toRun.size(); i++ ) {
            toRun[i].invoke();
        }
    }

    function getRedirectURL() {
        // Oauth in the SIM needs https :(
        if(System.getDeviceSettings().uniqueIdentifier.equals(Application.getApp().SIM_UID)) {
            return "https://localhost:8080";
        } else {
            return "https://localhost";
        }
    }

    function hasToken() as Boolean {
        return currentToken != null;
    }

    function grabToken() {
        // if not set, we need to do a full auth
        if(currentToken == null) {
            doAuth();
            return null;
        }

        // check if we need a token refresh
        if(Time.now().greaterThan(currentToken.expires_at)) {
            doRefresh();
            return null;
        }

        // eager start a new token
        if(currentToken.duration != null) {
            // 80% of the way towards expiry
            var eagerExpiry = currentToken.expires_at.subtract(currentToken.duration.divide(5)) as Moment;
            if(Time.now().greaterThan(eagerExpiry)) {
                doRefresh();
            }
        }

        return currentToken;
    }

    function doAuth() as Boolean {
        if(System.getDeviceSettings().phoneConnected == false) {
            return false;
        }

        var requestUID = randomBase64();
        Storage.setValue("requestUID", requestUID);
        Communications.makeOAuthRequest(
            "https://api.netatmo.com/oauth2/authorize",
            {
                "client_id" => $.NetatmoClientId,
                "redirect_uri" => getRedirectURL(),
                "response_type" => "code",
                "scope" => "read_station",
                "state" => requestUID
            },
            getRedirectURL(),
            Communications.OAUTH_RESULT_TYPE_URL,
            { "state" => "state", "code" => "code", "error" => "error" }
        );

        return true;
    }

    function onAuthCode(message as OAuthMessage) as Void {
        var requestUID = Storage.getValue("requestUID");
        if (message.data != null && requestUID != null) {
            var data = message.data as Dictionary;
            if(data["error"] == null && data["state"].equals(requestUID)) {
                Communications.makeWebRequest(
                    "https://api.netatmo.com/oauth2/token",
                    {
                        "grant_type" => "authorization_code",
                        "client_id" => $.NetatmoClientId,
                        "client_secret" => $.NetatmoClientSecret,
                        "redirect_uri" => getRedirectURL(),
                        "code" => data["code"],
                        "scope" => "read_station"
                    },
                    {
                        :method => Communications.HTTP_REQUEST_METHOD_POST
                    },
                    method(:onOauthToken)
                );
            } else {
                lastError = { "where" => "onAuthCode", "what" => "Data error", "why" => message.data };
            }
        } else {
            lastError = { "where" => "onAuthCode", "what" => "Missing data", "why" => message };
            System.println("OAuth Error");
            System.println(lastError);
        }
    }

    function onOauthToken(responseCode as Number, data as Null or Dictionary) as Void  {
        if (responseCode == 200) {
            currentToken = new Token({
                "access_token" => data["access_token"],
                "expires_in" => data["expires_in"],
                "refresh_token" => data["refresh_token"]
            });
            Storage.setValue("oAuthToken", currentToken.export());
            Storage.setValue("tokenErrorCount", 0);
            runAuthListeners();
        } else {
            lastError = { "where" => "onOauthToken", "what" => "Bad response", "why" => {"responseCode" => responseCode, "data" => "data"} };
            var lastErrorCount = Storage.getValue("tokenErrorCount");
            if (lastErrorCount == null) {
                lastErrorCount = 0;
            }
            Storage.setValue("tokenErrorCount", lastErrorCount + 1);

            // too many errors, reauth
            if (lastErrorCount > 3) {
                currentToken = null;
                Storage.setValue("oAuthToken", null);
            }
        }
    }

    function doRefresh() {
        if(System.getDeviceSettings().connectionAvailable == false) {
            return;
        }
        Communications.makeWebRequest(
            "https://api.netatmo.com/oauth2/token",
            {
                "grant_type" => "refresh_token",
                "client_id" => $.NetatmoClientId,
                "client_secret" => $.NetatmoClientSecret,
                "refresh_token" => currentToken.refresh_token,
            },
            {
                :method => Communications.HTTP_REQUEST_METHOD_POST
            },
            method(:onOauthToken)
        );
    }
}

(:glance)
class Token {
    var access_token as String;
    var refresh_token as String;
    var expires_at as Moment or Null;
    var duration as Duration or Null;

    function initialize(data as Dictionary) {
        access_token = data["access_token"];
        refresh_token = data["refresh_token"];
        if (data["expires_in"] != null) {
            duration = new Duration(data["expires_in"]);
        }
        if (data["expires_at"] != null) {
            expires_at = new Time.Moment(data["expires_at"]);
        } else if (data["expires_in"] != null) {
            expires_at = Time.now().add(duration);
        }
    }

    function export() as Dictionary {
        return { "access_token" => access_token, "refresh_token" => refresh_token, "expires_at" => expires_at.value(), "expires_in" => duration.value() };
    }
}