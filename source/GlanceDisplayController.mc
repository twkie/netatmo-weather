import Toybox.WatchUi;
import Toybox.Timer;
import Toybox.Lang;

(:glance)
class GlanceDisplayController {
    hidden var dTimer;

    function initialize() {
        dTimer = new Timer.Timer();
        Application.getApp().remote.addRequestListener("glance-controller", method(:onLoadingComplete));
    }

    function onLoadingComplete() {
        WatchUi.requestUpdate();
    }

    function start() {
        dTimer.start(method(:onTimer), 10 * 1000, true);
    }

    function onTimer() {
        WatchUi.requestUpdate();
    }

    function stop() {
        dTimer.stop();
    }
}