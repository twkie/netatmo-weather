import Toybox.Time;
import Toybox.Lang;

(:glance)
function getTimeAgo(moment as Moment) as String {
    var duration = Time.now().subtract(moment) as Duration;
    var secondsPassed = duration.value();
    var fiveMin = new Duration(Time.Gregorian.SECONDS_PER_MINUTE * 5);
    var oneHour = new Duration(Time.Gregorian.SECONDS_PER_HOUR);
    var oneDay = new Duration(Time.Gregorian.SECONDS_PER_DAY);
    var oneYear = new Duration(Time.Gregorian.SECONDS_PER_YEAR);
    if (duration.lessThan(fiveMin)) {
        return "Now";
    } else if(duration.lessThan(oneHour)) {
        return (secondsPassed / Time.Gregorian.SECONDS_PER_MINUTE).format("%d") + "m";
    } else if(duration.lessThan(oneDay)) {
        return (secondsPassed / Time.Gregorian.SECONDS_PER_HOUR).format("%d") + "h";
    } else if(duration.lessThan(oneYear)) {
        return (secondsPassed / Time.Gregorian.SECONDS_PER_DAY).format("%d") + "d";
    } else {
        return (secondsPassed / Time.Gregorian.SECONDS_PER_YEAR).format("%d") + "y";
    }
}