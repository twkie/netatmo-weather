import Toybox.Lang;
import Toybox.Time;

(:glance)
class WeatherData {
    hidden var rawData as Dictionary;

    const MODULE_TYPE_WEATHER_STATION = "NAMain";
    const MODULE_TYPE_OUTDOOR = "NAModule1";
    const MODULE_TYPE_ANEMOMETER = "NAModule2";
    const MODULE_TYPE_RAIN_GUAGE = "NAModule3";
    const MODULE_TYPE_INDOOR = "NAModule4";

    function initialize(data as Dictionary) {
        rawData = data;
    }

    function getStation() as Dictionary {
        var found = null;
        var devices = getDevices();
        for( var i = 0; i < devices.size(); i++ ) {
            if (devices[i]["type"].equals(MODULE_TYPE_WEATHER_STATION)) {
                found = devices[i];
                break;
            }
        }

        return found;
    }

    function getModules() as Array<Dictionary> {
        var station = getStation();
        if (station == null) {
            return [];
        }

        return station["modules"];
    }

    function getDevices() as Array<Dictionary> {
        var body = rawData["body"] as Dictionary;
        return body["devices"];
    }

    function getStationAsModule() as WeatherModule {
        return new WeatherModule(getStation());
    }

    function getInsideModules() as Array<WeatherModule> {
        var insideModules = [getStationAsModule()];
        var modules = getModules();

        for( var i = 0; i < modules.size(); i++ ) {
            if (modules[i]["type"].equals(MODULE_TYPE_INDOOR)) {
                insideModules.add(new WeatherModule(modules[i]));
            }
        }

        return insideModules;
    }

    function getOutsideModules() as Array<WeatherModule> {
        var modules = getModules();
        var outsideModules = [];
        var outsideTypes = [MODULE_TYPE_OUTDOOR, MODULE_TYPE_RAIN_GUAGE, MODULE_TYPE_ANEMOMETER];
        for( var i = 0; i < modules.size(); i++ ) {
            if (outsideTypes.indexOf(modules[i]["type"]) >= 0) {
                outsideModules.add(new WeatherModule(modules[i]));
            }
        }

        return outsideModules;
    }
}

(:glance)
class WeatherModule {
    hidden var rawData as Dictionary;
    var id as String; // mac address of the device
    var name as String; // name of the module
    var type as String; // type of the device
    var batteryPercent as Number; // Percentage of battery remaining (10=low)
    var time as Moment or Null; // when data was measured
    var temp as Number or Null; // temperature (in °C)
    var tempTrend as String or Null; // trend for the last 12h (up, down, stable)
    var pressure as Number or Null; // pressure in mbar
    var co2 as Number or Null; // CO2 level (in ppm)
    var humidity as Number or Null; // humidity (in %)
    var noise as Number or Null; // noise level (in dB)
    var rain as Number or Null; // rain in mm
    var sumRainDay as Number or Null; // rain measured for past 24h(mm)
    var sumRainHour as Number or Null; // rain measured for the last hour (mm)
    var windStrength as Number or Null; // wind strenght (km/h)
    var windAngle as Number or Null; // wind angle
    var gustStrengh as Number or Null; // gust strengh (km/h)
    var gustAngle as Number or Null; // gust angle

    function initialize(data as Dictionary) {
        rawData = data;
        id = data["id"];
        name = data["module_name"];
        // fallback to station_name if module_name is blank
        if (name == null) {
            name = data["station_name"];
        }
        type = data["type"];
        batteryPercent = data["battery_percent"];
        var dashboard_data = data["dashboard_data"] as Dictionary;
        if (dashboard_data["time_utc"] != null) {
            time = new Time.Moment(dashboard_data["time_utc"]);
        }
        if (dashboard_data["Temperature"] != null) {
            temp = dashboard_data["Temperature"];
        }
        if (dashboard_data["temp_trend"] != null) {
            tempTrend = dashboard_data["temp_trend"];
        }
        if (dashboard_data["Pressure"] != null) {
            pressure = dashboard_data["Pressure"];
        }
        if (dashboard_data["CO2"] != null) {
            co2 = dashboard_data["CO2"];
        }
        if (dashboard_data["Humidity"] != null) {
            humidity = dashboard_data["Humidity"];
        }
        if (dashboard_data["Noise"] != null) {
            noise = dashboard_data["Noise"];
        }
        if (dashboard_data["Rain"] != null) {
            rain = dashboard_data["Rain"];
        }
        if (dashboard_data["sum_rain_24"] != null) {
            sumRainDay = dashboard_data["sum_rain_24"];
        }
        if (dashboard_data["sum_rain_1"] != null) {
            sumRainHour = dashboard_data["sum_rain_1"];
        }
        if (dashboard_data["WindStrength"] != null) {
            windStrength = dashboard_data["WindStrength"];
        }
        if (dashboard_data["WindAngle"] != null) {
            windAngle = dashboard_data["WindAngle"];
        }
        if (dashboard_data["GustStrength"] != null) {
            gustStrengh = dashboard_data["GustStrength"];
        }
        if (dashboard_data["gustAngle"] != null) {
            gustAngle = dashboard_data["gustAngle"];
        }
    }
}