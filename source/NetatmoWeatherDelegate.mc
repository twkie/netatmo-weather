import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;

class NetatmoWeatherDelegate extends WatchUi.BehaviorDelegate {
    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onSwipe(event as WatchUi.SwipeEvent) as Boolean {
        var direction = event.getDirection();
        if (direction == WatchUi.SWIPE_RIGHT) {
            return Application.getApp().display.onPreviousTab();
        } else if (direction == WatchUi.SWIPE_LEFT) {
            return Application.getApp().display.onNextTab();
        }
        return false;
    }

    function onNextPage() as Boolean {
        return Application.getApp().display.onTogglePage(WatchUi.SLIDE_UP);
    }

    function onPreviousPage() as Boolean {
        return Application.getApp().display.onTogglePage(WatchUi.SLIDE_DOWN);
    }

    (:appsettings)
    function onMenu() {
        WatchUi.pushView(
            new SettingsMenu(),
            new SettingsMenuDelegate(),
            WatchUi.SLIDE_IMMEDIATE
        );

        return true;
    }
}