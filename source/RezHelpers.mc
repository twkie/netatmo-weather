import Toybox.Lang;
import Rez.Styles;

//! Function to see if a tap falls within the touch area for
//! a action menu.
//! @param x X coord of tap
//! @param y Y coord of tap
//! @return true if tapped, false otherwise
function isInActionArea(coord as Array<Numeric>) as Boolean {
    if (Styles.system_input__action_menu has :x1 &&
        Styles.system_input__action_menu has :y1 &&
        Styles.system_input__action_menu has :x2 &&
        Styles.system_input__action_menu has :y2) {

        var x = coord[0];
        var y = coord[1];

        if (x >= Styles.system_input__action_menu.x1 &&
            x <= Styles.system_input__action_menu.x2 &&
            y >= Styles.system_input__action_menu.y1 &&
            y <= Styles.system_input__action_menu.y2) {
            return true;
        }
    }
    return false;
}
