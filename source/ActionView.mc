import Toybox.WatchUi;
import Toybox.Lang;
import Toybox.Graphics;

class Action extends View {

    hidden var actionText;
    hidden var hideCallback;
    hidden var id;

    function initialize(text as String, context as String, closeCallback as Method() or Null) {
        View.initialize();

        actionText = text;
        hideCallback = closeCallback;
        id = context;
    }

    function getContext() {
        return id;
    }

    function onHide() {
        if(hideCallback != null) {
            hideCallback.invoke();
        }
    }

    function onUpdate(dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        var writer = new WrapText();
        var heightForCenter = dc.getFontHeight(Graphics.FONT_SMALL) + 2;
        writer.writeLines(dc, actionText, Graphics.FONT_SMALL, dc.getHeight() / 2 - heightForCenter);
    }
}