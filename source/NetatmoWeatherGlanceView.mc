import Toybox.WatchUi;
import Toybox.Graphics;
import Toybox.Lang;
import Toybox.Time;

(:glance)
class NetatmoWeatherGlanceView extends WatchUi.GlanceView {
    hidden var dTimer;

    function initialize() {
      GlanceView.initialize();
    }

    function onUpdate(dc) {
        var data = Application.getApp().remote.getDataOrCache();
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(0, dc.getHeight() / 4, Graphics.FONT_GLANCE, "NETATMO", Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);

        if (data != null) {
            renderOutsideInfo(dc, data);
            renderStationInfo(dc, data);
        } else {
            var noDataText = "Launch";
            if (Application.getApp().tokens.hasToken()) {
                noDataText = "Loading";
            }
            dc.drawText(0, dc.getHeight() * 3 / 4, Graphics.FONT_GLANCE, noDataText, Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);
        }
    }

    function renderOutsideInfo(dc, data) as Void {
        var twentyMinAgo = Time.now().subtract(new Time.Duration(20 * 60)) as Moment;
        var outsideModules = data.getOutsideModules() as Array<WeatherModule>;
        var outsideMod = outsideModules[0];
        if (outsideMod != null && outsideMod.temp != null) {
            // indicate data is old via a faded colour
            if (twentyMinAgo.greaterThan(outsideMod.time)) {
                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
            }

            dc.drawText(0, (dc.getHeight() * 3 / 4), Graphics.FONT_GLANCE, outsideMod.temp.format("%.1f") + "°", Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);
        } else {
            dc.drawText(0, (dc.getHeight() * 3 / 4), Graphics.FONT_GLANCE, "_._°", Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);
        }
    }

    function renderStationInfo(dc, data) as Void {
        var twentyMinAgo = Time.now().subtract(new Time.Duration(20 * 60)) as Moment;
        var stationMod = data.getStationAsModule() as WeatherModule;
        if (stationMod != null && stationMod.temp != null) {
            // indicate data is old via a faded colour
            if (twentyMinAgo.greaterThan(stationMod.time)) {
                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
            }

            var fetchTimeInfo = $.getTimeAgo(stationMod.time) as String;

            dc.drawText(dc.getWidth() - 40, (dc.getHeight() * 3 / 4), Graphics.FONT_GLANCE, stationMod.temp.format("%.1f") + "° " + fetchTimeInfo, Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER);
        } else {
            dc.drawText(dc.getWidth() - 40, (dc.getHeight() * 3 / 4), Graphics.FONT_GLANCE, "_._°", Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER);
        }
    }
}